import 'package:flutter/material.dart';
import 'package:animated_splash/animated_splash.dart';

void main() {
Function duringSplash = () {
	print('Something background process');
	int a = 123 + 23;
	print(a);

	if (a > 100)
	return 1;
	else
	return 2;
};

Map<int, Widget> op = {1: Home(), 2: HomeSt()};

runApp(MaterialApp(
	home: AnimatedSplash(
	imagePath: 'https://theeconomyng.com/mtn-rolls-out-momo-pay-on-its-mobile-money/', //change here
 	home: Home(),
	customFunction: duringSplash,
	duration: 3500,   //change
	type: AnimatedSplashType.BackgroundProcess,
	outputAndHome: op,
	),
));
}

class Home extends StatefulWidget {
@override
_HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
@override
Widget build(BuildContext context) {
	return Scaffold(
		appBar: AppBar(
		title: Text('Welcome to MTN MOMO SA'),  //change here
		backgroundColor: Colors.black,
		),
		body: Center(
			child: Text('My Home Page',
				style: TextStyle(color: Colors.black,
					fontSize: 15.0))));
}
}
