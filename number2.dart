MaterialApp(
 title: 'Welcome to MTN MOMO SA',  //change here
 theme: ThemeData(
 brightness: Brightness.dark,
 primaryColor: Colors.lightBlue[800],
 
 fontFamily: 'Hindi',    //change here
 textTheme: TextTheme(
  headline1: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
 ),
 ),
 home: HomeState(),
);
